var net        = require('net'),
    JSONStream = require('JSONStream'),
    util       = require('util');

var serverHost = process.argv[2],
    serverPort = process.argv[3],
    botName    = process.argv[4],
    botKey     = process.argv[5];

console.log('I\'m', botName, 'and connect to', serverHost + ':' + serverPort);

var FRICTION = 1.26,
    GEE = 9.81;

var trackData,
    originalTrackData,
    trackLanes,
    lapsCount,
    boostIndex,

    piece,
    nextPiece,

    maxAngle = 0,

    cleanFriction,
    crashed = false,
    carAccedent = false,

    distance          = 0,
    distancePrev      = 0,
    distanceIndex     = 0,
    distanceLastIndex = 0,

    switchSent = false,
    switchers,

    turbo = false,

    throttle = 1,
    carPosition,
    carPositionPrev,
    carPositionObj,
    carPositionObjPrev,
    speed = 0,
    globalData,
    message,
    pieceInfo,
    carPositionData,
    switcher = true,
    angleDiff;

var send = function(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
};

var piecesLength = function(index) {
    var length = 0,
        radius = 0;

    if(!trackData[index].radius) {
        length = trackData[index].length;
    } else {
        if(trackData[index].angle > 0) {
            radius = trackData[index].radius - trackLanes[carPosition.piecePosition.lane.startLaneIndex].distanceFromCenter;
        } else {
            radius = trackData[index].radius + trackLanes[carPosition.piecePosition.lane.startLaneIndex].distanceFromCenter;
        }
        length = Math.PI * radius * Math.abs(trackData[index].angle) / 180;
    }

    return length;
};

var getDistance = function(carPosition) {
    var pieceLength      = 0,
        carPositionIndex = carPosition.piecePosition.pieceIndex;

    if(carPositionIndex !== distanceLastIndex) {
        if(carPositionIndex - 1 >= 0) {
            pieceLength = piecesLength(carPositionIndex - 1);
        }

        if(trackData[carPositionIndex - 1] && trackData[carPositionIndex - 1].switch && carPositionPrev.piecePosition.lane.startLaneIndex !== carPositionPrev.piecePosition.lane.endLaneIndex) {
            distanceIndex = distanceIndex + pieceLength + 2.05;
        } else {
            distanceIndex = distanceIndex + pieceLength;
        }

        distanceLastIndex = carPositionIndex;
    }

    return distanceIndex + carPosition.piecePosition.inPieceDistance;
};

var getClosestSwitchers = function(index) {
    var firstSwitch,
        iterations = 0,
        i = index + 1;

    do {
        if(trackData[i] && trackData[i].switch) {
            if(firstSwitch) {
                return [firstSwitch, i];
            } else {
                firstSwitch = i;
            }
        }

        i = (i + 1) < trackData.length ? i + 1 : 0;
        iterations = iterations + 1;
    } while(iterations < trackData.length);
};

var extendTrackData = function(trackData) {
    var i, j, k, m,
        piecesData  = trackData.pieces,
        lanesData   = trackData.lanes,
        lanesLength = lanesData.length,
        trackLength = piecesData.length;

    var curves = [],
        curve = [];

    for(i = 0; i < trackLength; i++) {
        if(piecesData[i].radius) {
            piecesData[i].index = i;
            if(curve.length === 0 || curve[0].angle === piecesData[i].angle) {
                curve.push(piecesData[i]);
            } else {
                curves.push(curve);
                curve = [];
                curve.push(piecesData[i]);
            }
        } else {
            if(curve.length !== 0) {
                curves.push(curve);
                curve = [];
            }
        }
    }

    for(i = 0; i < curves.length; i++) {
        var apex = curves[i].length / 2;

        if(apex % 2 === 0) {
            for(j = 0; j < curves[i].length; j++) {
                if(j === apex) {
                    piecesData[curves[i][j].index].afterApex = true;
                }
            }
        } else {
            for(j = 0; j < curves[i].length; j++) {
                if(j === Math.ceil(apex)) {
                    piecesData[curves[i][j].index].afterApex = true;
                }
            }
        }
    }

    for(i = 0; i < trackLength; i++) {
        if(piecesData[i].radius) {
            piecesData[i].topSpeed = {};

            for(j = 0; j < lanesLength; j++) {
                if(piecesData[i].radius && piecesData[i].angle < 0) {
                    piecesData[i].topSpeed[j] = FRICTION * Math.sqrt(GEE * (piecesData[i].radius + lanesData[j].distanceFromCenter));
                } else {
                    piecesData[i].topSpeed[j] = FRICTION * Math.sqrt(GEE * (piecesData[i].radius - lanesData[j].distanceFromCenter));
                }
            }

            var prevPiece     = piecesData[i - 1],
                prevPrevPiece = piecesData[i - 2];

            if(prevPiece && prevPiece.length) {
                prevPiece.topSpeed = {};

                if(prevPrevPiece && prevPrevPiece.angle && prevPrevPiece.angle * piecesData[i].angle < 0) {
                    for(k = 0; k < lanesLength; k++) {
                        prevPiece.topSpeed[k] = piecesData[i].topSpeed[k];
                    }
                } else {
                    for(k = 0; k < lanesLength; k++) {
                        prevPiece.topSpeed[k] = 1.1 * piecesData[i].topSpeed[k];
                    }
                }
            }

            if(prevPrevPiece && prevPrevPiece.length && prevPiece.length) {
                prevPrevPiece.topSpeed = {};

                for(m = 0; m < lanesLength; m++) {
                    prevPrevPiece.topSpeed[m] = 1.1*prevPiece.topSpeed[m];
                }
            }

            if(prevPiece && Math.abs(prevPiece.angle) < Math.abs(piecesData[i].angle)) {
                prevPiece.STOOP = true;
                for(m = 0; m < lanesLength; m++) {
                    prevPiece.topSpeed[m] = piecesData[i].topSpeed[m];
                }
            }

            if(prevPiece && prevPiece.angle * piecesData[i].angle < 0) {
                prevPiece.CHANGE = true;
                for(m = 0; m < lanesLength; m++) {
                    prevPiece.topSpeed[m] = prevPrevPiece.topSpeed[m];
                }
            }

            if(prevPiece && prevPiece.angle && piecesData[i].angle && piecesData[i].angle * prevPiece.angle < 0) {
                if(prevPiece.topSpeed['0'] > piecesData[i].topSpeed['0']) {
                    for(m = 0; m < lanesLength; m++) {
                        prevPiece.topSpeed[m] = (piecesData[i].topSpeed[m] + prevPiece.topSpeed[m]) / 2;
                        if(prevPrevPiece && prevPrevPiece.topSpeed) {
                            prevPrevPiece.topSpeed[m] = (prevPiece.topSpeed[m] + prevPrevPiece.topSpeed[m]) / 2;
                        }
                    }
                }
            }
        }
    }

    return piecesData;
};

// client = net.connect(serverPort, serverHost, function() {
//     return send({
//         msgType: 'createRace',
//         data: {
//             botId: {
//                 name: botName,
//                 key: botKey
//             },
//             trackName: 'keimola',
//             carCount: 1
//         }
//     });
// });

client = net.connect(serverPort, serverHost, function() {
    return send({
        msgType: 'join',
        data: {
            name: botName,
            key: botKey
    }});
});

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
    switch(data.msgType) {
        case 'gameInit':
            message    = 'gameInit';
            trackLanes = data.data.race.track.lanes;
            lapsCount  = data.data.race.raceSession.laps - 1;

            originalTrackData = data.data.race.track;

            trackData = extendTrackData(data.data.race.track);

            var curvePiece = false;
            var ii = trackData.length - 1;
            do {

                if(trackData[ii].angle) {
                    curvePiece = true;
                    boostIndex = ii;
                }
                ii = ii - 1;
            } while (!curvePiece);


            console.log(util.inspect(data, {showHidden: false, depth: null}));
            break;
        case 'gameStart':
            message = 'gameStart';
            send({
                msgType: 'throttle',
                data: 1
            });
            break;
        case 'carPositions':
            console.log(util.inspect(data, {showHidden: false, depth: null}));
            carPositionObj = data;
            message        = 'carPositions';
            for(var q = 0; q < data.data.length; q++) {
                if(data.data[q].id.name === botName) {
                    carPosition = data.data[q];
                    continue;
                }
            }

            piece     = trackData[carPosition.piecePosition.pieceIndex];
            nextPiece = trackData.length === (carPosition.piecePosition.pieceIndex + 1) ? trackData[0] : trackData[carPosition.piecePosition.pieceIndex + 1];

            if(Math.abs(carPosition.angle) > maxAngle) {
                maxAngle = Math.abs(carPosition.angle);
            }

            if(carPositionPrev) {
                distancePrev = distance;
                distance     = getDistance(carPosition);

                for(var w = 0; w < data.data.length; w++) {
                    if(data.data[w].id.name !== botName) {

                        if(carPosition.piecePosition.pieceIndex === data.data[w].piecePosition.pieceIndex &&
                                carPosition.piecePosition.lane.startLaneIndex === data.data[w].piecePosition.lane.startLaneIndex &&
                                carPosition.piecePosition.lane.endLaneIndex === data.data[w].piecePosition.lane.endLaneIndex) {
                            if(data.data[w].piecePosition.inPieceDistance - carPosition.piecePosition.inPieceDistance <= 45 &&
                                    data.data[w].piecePosition.inPieceDistance - carPosition.piecePosition.inPieceDistance > 0 &&
                                    nextPiece.switch) {
                                carAccedent = true;
                                continue;
                            }
                        }
                    }
                }

                speed        = (distance - distancePrev) / (carPositionObj.gameTick - carPositionObjPrev.gameTick) * 6;

                // +1: car's angle is increasing
                // -1: car's angle is decreasing
                angleDiff    = Math.abs(carPosition.angle) > Math.abs(carPositionPrev.angle) ? 1 : -1;
            }

            if (piece.length && !nextPiece.length) {
                pieceInfo = 'from straight to turn';

                if(piece.topSpeed && speed > piece.topSpeed[carPosition.piecePosition.lane.startLaneIndex]) {
                    throttle = 0;
                } else {
                    throttle = 1;
                }
            } else if (piece.length && nextPiece.length) {
                pieceInfo = 'straight';

                if(piece.topSpeed && speed > piece.topSpeed[carPosition.piecePosition.lane.startLaneIndex]) {
                    throttle = 0;
                } else {
                    throttle = 1;
                }

            } else if (!piece.length && nextPiece.length) {
                pieceInfo = 'from turn to straight';

                if(speed > piece.topSpeed[carPosition.piecePosition.lane.startLaneIndex]) {
                    throttle = 0;
                } else {
                    throttle = 1;
                }
            } else if (!piece.length && !nextPiece.length) {
                pieceInfo = 'inside turn';


                if((carPosition.piecePosition.inPieceDistance < piecesLength(carPosition.piecePosition.pieceIndex) / 3) && speed > piece.topSpeed[carPosition.piecePosition.lane.startLaneIndex]) {
                    throttle = 0;
                } else {
                    if(piece.topSpeed && speed > piece.topSpeed[carPosition.piecePosition.lane.startLaneIndex]) {
                        if(piece.afterApex && (nextPiece.length || nextPiece.angle <= piece.angle) && nextPiece.angle * piece.angle > 0) {
                            throttle = 1;
                        } else {
                            throttle = 0;
                        }
                    } else {
                        throttle = 1;
                    }
                }
            }

            if(piece.topSpeed && nextPiece.topSpeed) {
                if(piece.topSpeed[carPosition.piecePosition.pieceIndex] - nextPiece.topSpeed[carPosition.piecePosition.pieceIndex] > 10) {
                    piece.topSpeed[carPosition.piecePosition.pieceIndex] = nextPiece.topSpeed[carPosition.piecePosition.pieceIndex];
                }
            }

            if(switcher) {
                switchers = getClosestSwitchers(carPosition.piecePosition.pieceIndex);

                var minusRadius    = [],
                    plusRadius     = [],
                    minusRadiusSum = 0,
                    plusRadiusSum  = 0,
                    degrees        = 0;
                for(var k = switchers[0] + 1; k < switchers[1]; k++) {
                    if(trackData[k].angle) {

                        degrees = degrees + trackData[k].angle;
                        if(trackData[k].angle < 0) {
                            minusRadius.push(trackData[k].radius);
                            minusRadiusSum = minusRadiusSum + trackData[k].radius;
                        } else {
                            plusRadius.push(trackData[k].radius);
                            plusRadiusSum = plusRadiusSum + trackData[k].radius;
                        }
                    }
                }

                if(carAccedent) {
                    degrees = degrees * (Math.random() * 2 - 1);
                }

                carAccedent = false;

                if(degrees > 0 && nextPiece.switch) {
                    send({
                        msgType: 'switchLane',
                        data:    'Right'
                    });
                    switchSent = true;
                    switcher   = false;
                } else if(degrees < 0 && nextPiece.switch) {
                    send({
                        msgType: 'switchLane',
                        data:    'Left'
                    });
                    switchSent = true;
                    switcher   = false;
                }
            }

            if(nextPiece.switch && !switchSent) {
                switcher = true;
            } else if(!nextPiece.switch && piece.switch) {
                switchSent = false;
            }

            if(turbo && carPosition.piecePosition.lap === lapsCount && carPosition.piecePosition.pieceIndex > boostIndex) {
                send({
                    msgType: 'turbo',
                    data:    'Ooh Yeaaaah!!!'
                });
                turbo = false;
            }

            send({
                msgType: 'throttle',
                data: throttle
            });

            globalData      = data;
            carPositionData = data;

            carPositionPrev    = carPosition;
            carPositionObjPrev = carPositionObj;
            break;
        case 'crash':
            if(data.data.name !== botName) {
                break;
            }
            message = 'crash';
            if(Math.abs(carPosition.angle) > 56) {

                if(cleanFriction && cleanFriction === FRICTION) {
                    cleanFriction = cleanFriction - 0.01;
                }

                FRICTION = cleanFriction || FRICTION - 0.05;
                extendTrackData(originalTrackData);
                carAccedent = false;
                crashed = true;
            } else {
                carAccedent = true;
            }
            console.log(util.inspect(data, {showHidden: false, depth: null}));
            break;
        case 'spawn':
            message = 'spawn';
            break;
        case 'lapFinished':
            if(data.data.car.name !== botName) {
                break;
            }
            message    = 'lapFinished';
            globalData = data;

            if(!crashed) {
                cleanFriction = FRICTION;
            }

            if(!crashed && maxAngle < 52) {
                if(maxAngle <= 30) {
                    FRICTION = FRICTION + 0.08;
                } else if(maxAngle > 30 && maxAngle <= 40) {
                    FRICTION = FRICTION + 0.06;
                } else if(maxAngle > 40 && maxAngle <=50) {
                    FRICTION = FRICTION + 0.04;
                } else {
                    FRICTION = FRICTION + 0.01;
                }
                extendTrackData(originalTrackData);
                maxAngle = 0;
            }

            if(!crashed) {
                maxAngle = 0;
            }
            console.log(util.inspect(data, {showHidden: false, depth: null}));
            crashed = false;

            break;
        case 'dnf':
            message = 'dnf';
            break;
        case 'finish':
            message = 'finish';
            break;
        case 'gameEnd':
            message = 'gameEnd';
            break;
        case 'turboAvailable':
            turbo = true;
            break;
        default:
            message    = 'Unexpected Message';
            globalData = data;
            console.log(util.inspect(data, {showHidden: false, depth: null}));
    }

    console.log('=========================================================');
    console.log('Message: ', message, 'friction: ', FRICTION, 'maxAngle', maxAngle);
    console.log('Distance:', distance.toFixed(2), ', speed: ', speed.toFixed(2));
    console.log('Trottle: ', throttle.toFixed(2), ', angleDiff: ', angleDiff);
    if(carPosition) {
        console.log('Message: ', message, 'lane', carPosition.piecePosition.lane, 'carAccedent', carAccedent);
        console.log('Position: angle: ', carPosition.angle.toFixed(2));
        console.log('Position: index: ', carPosition.piecePosition.pieceIndex, ', distance: ', carPosition.piecePosition.inPieceDistance.toFixed(2));
        console.log('Lap: ', carPosition.piecePosition.lap, ', gameTick: ', carPosition.gameTick);
    }
    console.log('Position info:   ', pieceInfo);
    console.log('Current piece: ', piece, ', switchers:', switchers, ', turbo: ', turbo);
    if(trackData && carPosition && trackData.length !== carPosition.piecePosition.pieceIndex) {
        console.log('Next piece:    ', nextPiece);
    }
});

jsonStream.on('error', function() {
    return console.log('===LOG=== Disconnected');
});
